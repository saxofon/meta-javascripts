SUMMARY = "A renderer agnostic two-dimensional drawing api for the web"
DESCRIPTION = "Javascript lib for drawing on canvas, webgl etc in 2D"
HOMEPAGE = "https://github.com/jonobr1/two.js"
SECTION = "js"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e1413916c95a3e12e924a25d3207adf5"

SRC_URI = "git://github.com/jonobr1/two.js.git;protocol=https;branch=dev"
SRC_URI[md5sum] = "492b276601172fe3b6fda5aac9d60a70"
SRCREV = "73d9b884adfdae79699ad09c06fab870067faaa3"

S = "${WORKDIR}/git"

FILES_${PN} = "/usr/lib/js/two"

do_compile() {
	:
}

do_install() {
	install -m 755 -d ${D}/usr/lib/js/two
	cp ${S}/build/two.min.js ${D}/usr/lib/js/two
	find ${D}/usr/lib/js/two -type f -exec chmod 644 {} \;
}
