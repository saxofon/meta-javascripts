LIC_FILES_CHKSUM = "file://LICENSE;md5=37b5762e07f0af8c74ce80a8bda4266b"

SRCREV = "52b2447247f535663ac1c292e088b4b27d2910ef"

PV = "3.9.2+git${SRCPV}"

SRC_URI = "git://github.com/google/protobuf.git;branch=3.9.x"

require protobuf-js.inc
