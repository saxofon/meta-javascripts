SUMMARY = "Protocol Buffers - structured data serialisation mechanism"
DESCRIPTION = "Protocol Buffers are a way of encoding structured data in an \
efficient yet extensible format. Google uses Protocol Buffers for almost \
all of its internal RPC protocols and file formats."
HOMEPAGE = "https://github.com/google/protobuf"
SECTION = "console/tools"
LICENSE = "BSD-3-Clause"

S = "${WORKDIR}/git"

BBCLASSEXTEND = "native nativesdk"

FILES_${PN} += "/usr/lib/js/protobuf-js"

do_compile() {
	:
}

do_install() {
	mkdir -p ${D}/usr/lib/js/protobuf-js/binary
	mkdir -p ${D}/usr/lib/js/protobuf-js/commonjs
	cp ${S}/js/*.js ${D}/usr/lib/js/protobuf-js
	cp ${S}/js/binary/*.js ${D}/usr/lib/js/protobuf-js/binary
	cp ${S}/js/commonjs/*.js ${D}/usr/lib/js/protobuf-js/commonjs
	rm ${D}/usr/lib/js/protobuf-js/*_test.js
	rm ${D}/usr/lib/js/protobuf-js/binary/*_test.js
	rm ${D}/usr/lib/js/protobuf-js/commonjs/*_test.js
}
