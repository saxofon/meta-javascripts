LIC_FILES_CHKSUM = "file://LICENSE;md5=35953c752efc9299b184f91bef540095"

SRCREV = "106ffc04be1abf3ff3399f54ccf149815b287dd9"

PV = "3.5.1+git${SRCPV}"

SRC_URI = "git://github.com/google/protobuf.git;branch=3.5.x"

require protobuf-js.inc
