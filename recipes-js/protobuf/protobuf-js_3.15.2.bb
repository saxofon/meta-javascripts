LIC_FILES_CHKSUM = "file://LICENSE;md5=37b5762e07f0af8c74ce80a8bda4266b"

SRCREV = "d7e943b8d2bc444a8c770644e73d090b486f8b37"

PV = "3.15.2+git${SRCPV}"

SRC_URI = "git://github.com/google/protobuf.git;branch=3.15.x"

require protobuf-js.inc
