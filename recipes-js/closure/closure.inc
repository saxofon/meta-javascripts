SUMMARY = "Google's common JavaScript library"
DESCRIPTION = "Google's common JavaScript library"
HOMEPAGE = "https://github.com/google/closure-library.git"
SECTION = "js"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e23fadd6ceef8c618fc1c65191d846fa"

SRC_URI = "git://github.com/google/closure-library.git;protocol=https"
SRC_URI[md5sum] = "dce0246f1f2e89ae497ea740289573a4"

S = "${WORKDIR}/git"

FILES_${PN} = "/usr/lib/js/closure"

do_compile() {
	:
}

do_install() {
	install -m 755 -d ${D}/usr/lib/js
	cp -r ${S}/closure ${D}/usr/lib/js
	find ${D}/usr/lib/js -type f -exec chmod 644 {} \;
}
